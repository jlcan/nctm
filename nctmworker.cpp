/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#include "nctmworker.h"

NCTNMWorker::NCTNMWorker(QObject *parent) : QObject(parent)
{

}

void NCTNMWorker::process_micrograph(QString & istr)
{
  QProgressDialog progressDialog;
  progressDialog.setLabelText(QString("Processing..."));

  QObject::connect(&ftWatcher, &QFutureWatcher<void>::finished,
                   &progressDialog, &QProgressDialog::reset);
  QObject::connect(&progressDialog, &QProgressDialog::canceled,
                   &ftWatcher, &QFutureWatcher<void>::cancel);
  QObject::connect(&ftWatcher, &QFutureWatcher<void>::progressRangeChanged,
                   &progressDialog, &QProgressDialog::setRange);
  QObject::connect(&ftWatcher, &QFutureWatcher<void>::progressValueChanged,
                   &progressDialog, &QProgressDialog::setValue);

  ftWatcher.setFuture(
        QtConcurrent::run(this, &NCTNMWorker::run_singleFile, istr)
        );

  progressDialog.exec();
  ftWatcher.waitForFinished();
}

void NCTNMWorker::process_micrograph(QStringList & istrl)
{
  QProgressDialog progressDialog;
  progressDialog.setLabelText(QString("Processing..."));

  QObject::connect(&ftWatcher, &QFutureWatcher<void>::finished,
                   &progressDialog, &QProgressDialog::reset);
  QObject::connect(&progressDialog, &QProgressDialog::canceled,
                   &ftWatcher, &QFutureWatcher<void>::cancel);
  QObject::connect(&ftWatcher, &QFutureWatcher<void>::progressRangeChanged,
                   &progressDialog, &QProgressDialog::setRange);
  QObject::connect(&ftWatcher, &QFutureWatcher<void>::progressValueChanged,
                   &progressDialog, &QProgressDialog::setValue);

  ftWatcher.setFuture(
        QtConcurrent::run(this, &NCTNMWorker::run_batch, istrl)
        );

  progressDialog.exec();
  ftWatcher.waitForFinished();
}

int NCTNMWorker::run_singleFile(QString istr)
{
  int retval;

  retval = process_single_file(istr.toStdString());

  return retval;
}

int NCTNMWorker::run_batch(QStringList istrl)
{
  int retval = 0;

  foreach (auto litem, istrl)
    {
      retval = process_single_file(litem.toStdString());
    }

  return retval;
}
