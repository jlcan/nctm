/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#ifndef NCTM_COMMON_H
#define NCTM_COMMON_H
#include <iostream>
#include <string>
#include <vector>
#include <exception>
#include <stdexcept>

#include <QDebug>

#ifndef  OCV_LIBS
#define OCV_LIBS
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
using namespace cv;
#endif

struct detectedObjects {
  cv::Rect bbox;
  std::string label;
  float confidence;
};

const cv::Scalar MAGENTA(255, 0, 255);
const cv::Scalar GREEN(0, 255, 0);
const cv::Scalar BLUE(255, 150, 0);
const cv::Scalar YELLOW(0,255,255);
const cv::Scalar CYAN(255,255,0);
const cv::Scalar WHITE(255,255,255);

#endif
