/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#include "nctm.hpp"

void saveReport(Micrograph & mc)
{
  std::vector<std::string> report_list = mc.getReportList();
  std::vector<std::string> imagepath_list = mc.nanoparticlesPathList();
  std::string globalrep = mc.getReportStr();
  std::string outputdir = mc.outputDir();

  //for (auto & p : imagepath_list)
  //  

  printReport(report_list, imagepath_list, globalrep, outputdir);
}

int process_single_file(std::string  istr)
{
  setupLogs();
  std::cout << "***** Processing micrograph " << istr << " ***** \n" << std::endl;
  std::cout << "1) Initiating main regions detection" << std::endl;
  Micrograph mc(istr);
  std::string outdir = createDirectory(istr);
  std::string basename = getBasename(istr);
  mc.setOutputDir(outdir);
  mc.setBasename(basename);
  cv::Mat mic = mc.getOriginalImg();

  std::cout << "2) Initiating infobar detection with CNN" << std::endl;
  std::vector<detectedObjects> detected_objs;
  detected_objs = dnndetect(mic,
                          getConfigDir() + PATH_SEPARATOR + CNN_BAR_MODEL,
                          getConfigDir() + PATH_SEPARATOR + CNN_BAR_CONFIG,
                          getConfigDir() + PATH_SEPARATOR + CNN_BAR_CLASSES);

  cv::Mat simg = mc.getOriginalImg();
  cv::Size sz = simg.size();

  scaledata scd;

  processBarDetection(detected_objs, sz);
  if (detected_objs.size() >= 2 && detected_objs.size() < 4)
    {
      mc.setMainROIs(detected_objs);
      mc.saveData();
      std::string ocr_img_path = mc.saveImgForOCR(".pgm", 0);
      scd = extractInfobarData(ocr_img_path);
      if (scd.magnitude == -1 || scd.meas_unit == "Null")
        {
          ocr_img_path = mc.saveImgForOCR(".pgm", 1);
          scd = extractInfobarData(ocr_img_path);
        }
      mc.setScaledata(scd);
      std::cout << "scd = " << scd.magnitude << "_" << scd.meas_unit << std::endl;
      for(auto & obj : detected_objs)
        {
          
          if(obj.label == "scalebar-I-Beam" || obj.label == "scalebar-simple" || obj.label == "scalebar-pillars")
            mc.setScalebarType(obj.label);
          /*if(obj.label == "scalebar-I-Beam")
            mc.setScalebarType(obj.label);
          if()
            mc.setScalebarType(obj.label);*/
        }
      mc.doScalebarDetection();
    }
  else
    {
      std::cout << detected_objs.size() << " objects detected" << std::endl;
      mc.setMainROIs(detected_objs);
      mc.saveData();
    }

  // NP detection
  std::cout << "3) Initiating nanoparticle detection" << std::endl;
  detected_objs.clear(); // clear detected objects vector @TODO remove global variable
  cv::Mat img_clean = mc.getSceneImg();
  detected_objs = dnndetect(img_clean,
                          getConfigDir() + PATH_SEPARATOR + CNN_MODEL,
                          getConfigDir() + PATH_SEPARATOR + CNN_CONFIG,
                          getConfigDir() + PATH_SEPARATOR + CNN_CLASSES);

  std::cout << "nps = " << detected_objs.size() << std::endl;
  if (detected_objs.empty())
    {
      std::cout << "No nanoparticles were detected, exiting..." << std::endl;
      mc.saveData();
      mc.generateGlobalReport();
      saveReport(mc);
      return -1;
    }

  mc.reserveNPMemory(detected_objs.size());
  for (auto & det : detected_objs)
    {
      mc.addNP(det.bbox, det.label, det.confidence);
      //detected_objs.erase(detected_objs.begin()); // this causes "unidentified" NPs
    }
  detected_objs.clear();

  // Morphological characterization
  std::cout << "4) Initiating morphological characterization" << std::endl;
  mc.doMorph();

  // save n' exit
  mc.saveData();
  mc.saveResults();

  saveReport(mc);

  return 0;
}
