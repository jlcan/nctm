/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#include "fileoperations.hpp"

QString getHomeDir()
{
  QDir home = QDir::home();
  return home.absolutePath();
}

std::string getConfigDir()
{
  QDir configpath = QDir(getHomeDir() + CONFIG_DIR_NAME);
  return configpath.absolutePath().toStdString();
}

void setupLogs()
{
  QString logfile = getHomeDir() + QDir::separator() + NCTM_OUTDIRNAME + QDir::separator() + LOG_FILE;
  freopen( logfile.toStdString().c_str(), "a", stdout );
  QString errorfile = getHomeDir() + QDir::separator() + NCTM_OUTDIRNAME + QDir::separator() + ERR_LOG_FILE;
  freopen( errorfile.toStdString().c_str(), "a", stderr );
}

std::string createDirectory(std::string & dirname)
{
  QFileInfo fi(QString::fromUtf8(dirname.c_str()));
  QString basename = fi.baseName();

  char* home_env = getenv(HOME_VAR_NAME);
  QString home = home_env;
  QString outpath = home + QDir::separator() + NCTM_OUTDIRNAME + QDir::separator() + basename;
  QDir outdir;
  outdir.mkpath(outpath);
  outdir.mkpath(outpath + QDir::separator() +"NPs");

  return outpath.toStdString();
}

std::string getBasename(std::string & str)
{
  QFileInfo fi(QString::fromUtf8(str.c_str()));
  QString basename = fi.baseName();
  std::string st = basename.toStdString();
  return st;
}

void printReport(std::vector<std::string> reportStrL, std::vector<std::string> imagepathL, std::string globalRepStr,std::string & outdir)
{
  QPrinter printer(QPrinter::HighResolution);
  QPainter painter;
  QFont font;

  printer.setOutputFormat(QPrinter::PdfFormat);
  printer.setPageSize(QPrinter::Letter);
  printer.setOrientation(QPrinter::Portrait);
  printer.setPageMargins(10, 10, 10, 10, QPrinter::Millimeter);
  printer.setOutputFileName(QString::fromStdString(outdir + PATH_SEPARATOR + "results.pdf"));

  int y = 0;
  const int pagerows = 4;
  int step = std::floor(printer.height()/pagerows);
  QRect textrect = QRect(0, y, printer.width()/2, printer.height()/pagerows);
  QRect imgrect = QRect(printer.width()/2, y, printer.width()/2, printer.height()/pagerows);

  painter.begin(&printer);
  font = painter.font();
  font.setPixelSize(128);
  painter.setFont(font);

  QRect gboundingRect;
  painter.drawText(textrect, Qt::AlignLeft,QString::fromStdString(globalRepStr), &gboundingRect);


  QImage img;


  if (reportStrL.size() > 0)
    {
      printer.newPage();
      for (size_t i = 0; i < reportStrL.size(); i++)
        {

          QRect boundingRect;
          QImageReader imgreader(QString::fromStdString(imagepathL.at(i)));
          img = imgreader.read();

          if (i > 0 && i % pagerows == 0)
            {
              printer.newPage();
              y = 0;
              textrect.setLeft(0);
              textrect.setTop(y);
              textrect.setWidth(printer.width()/2);
              textrect.setHeight(step);

              imgrect.setLeft(printer.width()/2);
              imgrect.setTop(y);
              imgrect.setWidth(printer.width()/2);
              imgrect.setHeight(step);
            }

          painter.drawText(textrect, Qt::AlignLeft, QString::fromStdString(reportStrL.at(i)), &boundingRect);
          painter.drawImage(imgrect, img);
          y += step;

          textrect.setLeft(0);
          textrect.setTop(y);
          textrect.setWidth(printer.width()/2);
          textrect.setHeight(step);

          imgrect.setLeft(printer.width()/2);
          imgrect.setTop(y);
          imgrect.setWidth(printer.width()/2);
          imgrect.setHeight(step);
        }
    }
  painter.end();
}
