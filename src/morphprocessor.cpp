/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

// morphprocessor.hpp -- morphological characterization

#include "morphprocessor.hpp"

cv::Rect fitCropRegion(cv::Rect region, cv::Size maxDims)
{
  std::cout << "max dimensions allowed " << maxDims << std::endl;

  if (region.x + region.width > maxDims.width)
    {
      std::cout << "too wide!" << region.x + region.width << std::endl;
      region.width = std::abs((region.x + region.width) - maxDims.width);
    }
  if (region.y + region.height > maxDims.height)
    {
      std::cout << "too tall!" << region.y + region.height << std::endl;
      region.height = std::abs((region.y + region.height) - maxDims.height);
    }
  if (region.x < 0) region.x = 0;
  if (region.y < 0) region.y = 0;

  return region;
}

cv::Rect increaseCropRegion(cv::Rect region, cv::Size maxDims)
{
  float sf = 1.15;
  unsigned cx,cy,nx,ny,nw,nh;
  cv::Rect new_region;

  cx = region.width/2;
  cy = region.height/2;
  nw = region.width*sf;
  nh = region.height*sf;
  nx = region.x - (nw/2 - cx);
  ny = region.y - (nh/2 - cy);

  new_region = cv::Rect(nx, ny, nw, nh);
  new_region = fitCropRegion(new_region, maxDims);

  return new_region;
}

std::vector<cv::Point> findMajorAxis(std::vector<cv::Point> contours)
{
  int maxdist = 0;
  int kdist;
  cv::Point p1_mAx,p2_mAx;
  std::vector<cv::Point> majorAxis;

  for(size_t i = 0; i < contours.size(); i++)
    {
      for(size_t k = 0; k < contours.size(); k++)
        {
          kdist = sqrt(pow(contours[i].x - contours[k].x,2) + pow(contours[i].y - contours[k].y,2));
          if (kdist >= maxdist)
            {
              maxdist = kdist;
              p1_mAx = contours[i];
              p2_mAx = contours[k];
            }
        }
    }
  //std::cout << "Major axis has length " << maxdist << " and between " << p1_mAx << ":" << p2_mAx << std::endl;
  majorAxis.push_back(p1_mAx);
  majorAxis.push_back(p2_mAx);
  return majorAxis;
}

std::vector<cv::Point> findMinorAxis(std::vector<cv::Point> contours, std::vector<cv::Point> majorAxis)
{
  int maxdist = 0;
  int kdist;
  cv::Point p1_minAx,p2_minAx;
  std::vector<cv::Point> minorAxis;
  std::vector<std::vector<cv::Point>> nearPerpendicular;
  double kslope;
  double difference = 0.05,kdifference;
  double minorAxis_slope;
  long double majorAxis_slope = (double) (majorAxis.at(1).y-majorAxis.at(0).y) / (majorAxis.at(1).x-majorAxis.at(0).x);

  for(size_t i = 0; i < contours.size(); i++)
    {
      for(size_t k = 0; k < contours.size(); k++)
        {
          kdist = sqrt(pow(contours[i].x - contours[k].x,2) + pow(contours[i].y - contours[k].y,2));
          minorAxis_slope = (double) (contours[k].y - contours[i].y) / (contours[k].x - contours[i].x);
          kslope = minorAxis_slope * majorAxis_slope;

          if (kslope < 0)
            {
              kdifference = std::abs(-1 - (kslope));
              if (kdifference < difference)
                {
                  std::vector<cv::Point> nper_line;
                  nper_line.push_back(contours[i]);
                  nper_line.push_back(contours[k]);
                  nearPerpendicular.push_back(nper_line);
                  //std::cout << " m1 . m2=" << kslope << std::endl;
                }
            }
        }
    }

  for(size_t i = 0; i < nearPerpendicular.size(); i++)
    {
      kdist = sqrt(pow(nearPerpendicular.at(i).at(0).x - nearPerpendicular.at(i).at(1).x,2) + pow(nearPerpendicular.at(i).at(0).y - nearPerpendicular.at(i).at(1).y,2));
      if (kdist >= maxdist)
        {
          maxdist = kdist;
          p1_minAx = nearPerpendicular.at(i).at(0);
          p2_minAx = nearPerpendicular.at(i).at(1);
        }
    }
  //std::cout << "Minor axis has length " << maxdist << " and between " << p1_minAx << ":" << p2_minAx << std::endl;
  minorAxis.push_back(p1_minAx);
  minorAxis.push_back(p2_minAx);
  //std::cout << minorAxis.size() << std::endl;
  return minorAxis;
}

void drawAxis(cv::Mat& img, cv::Point p, cv::Point q, cv::Scalar colour, const float scale = 0.2)
{
  double angle = std::atan2( (double) p.y - q.y, (double) p.x - q.x );
  double hypotenuse = std::sqrt( (double) (p.y - q.y) * (p.y - q.y) + (p.x - q.x) * (p.x - q.x));

  q.x = (int) (p.x - scale * hypotenuse * cos(angle));
  q.y = (int) (p.y - scale * hypotenuse * sin(angle));
  cv::line(img, p, q, colour, 1, cv::LINE_AA);

  p.x = (int) (q.x + 9 * cos(angle + CV_PI / 4));
  p.y = (int) (q.y + 9 * sin(angle + CV_PI / 4));
  cv::line(img, p, q, colour, 1, cv::LINE_AA);
  p.x = (int) (q.x + 9 * cos(angle - CV_PI / 4));
  p.y = (int) (q.y + 9 * sin(angle - CV_PI / 4));
  cv::line(img, p, q, colour, 1, cv::LINE_AA);
}

double getOrientation(const std::vector<cv::Point> &pts, cv::Mat &img)
{
  //Construct a buffer used by the pca analysis
  int sz = static_cast<int>(pts.size());
  cv::Mat data_pts = cv::Mat(sz, 2, CV_64F);
  for (int i = 0; i < data_pts.rows; i++)
    {
      data_pts.at<double>(i, 0) = pts[i].x;
      data_pts.at<double>(i, 1) = pts[i].y;
    }


  cv::PCA pca_analysis(data_pts, cv::Mat(), cv::PCA::DATA_AS_ROW);


  cv::Point cntr = cv::Point(static_cast<int>(pca_analysis.mean.at<double>(0, 0)),
                             static_cast<int>(pca_analysis.mean.at<double>(0, 1)));


  std::vector<cv::Point2d> eigen_vecs(2);
  std::vector<double> eigen_val(2);
  for (int i = 0; i < 2; i++)
    {
      eigen_vecs[i] = cv::Point2d(pca_analysis.eigenvectors.at<double>(i, 0),
                                  pca_analysis.eigenvectors.at<double>(i, 1));
      eigen_val[i] = pca_analysis.eigenvalues.at<double>(i);
    }


  circle(img, cntr, 3, cv::Scalar(255, 0, 255), 2);
  cv::Point p1 = cntr + 0.02 * cv::Point(static_cast<int>(eigen_vecs[0].x * eigen_val[0]), static_cast<int>(eigen_vecs[0].y * eigen_val[0]));
  cv::Point p2 = cntr - 0.02 * cv::Point(static_cast<int>(eigen_vecs[1].x * eigen_val[1]), static_cast<int>(eigen_vecs[1].y * eigen_val[1]));
  drawAxis(img, cntr, p1, cv::Scalar(0, 255, 0), 1);
  drawAxis(img, cntr, p2, cv::Scalar(255, 255, 0), 5);
  double angle = atan2(eigen_vecs[0].y, eigen_vecs[0].x); // orientation in radians
  return angle;
}

cv::Point getCenter(const std::vector<cv::Point> & pts)
{
  int sz = static_cast<int>(pts.size());
  cv::Mat data_pts = cv::Mat(sz, 2, CV_64F);

  for (int i = 0; i < data_pts.rows; i++)
    {
      data_pts.at<double>(i, 0) = pts[i].x;
      data_pts.at<double>(i, 1) = pts[i].y;
    }

  cv::PCA pca_analysis(data_pts, cv::Mat(), cv::PCA::DATA_AS_ROW);
  cv::Point cntr = cv::Point(static_cast<int>(pca_analysis.mean.at<double>(0, 0)),
                             static_cast<int>(pca_analysis.mean.at<double>(0, 1)));

  return cntr;
}

float distanceBetPoints(cv::Point & p1, cv::Point & p2)
{
  return std::sqrt(pow(p2.x-p1.x, 2) + pow(p2.y-p2.y, 2));
}
