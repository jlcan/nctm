/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#include "infobarprocessor.hpp"

using namespace std;

void invertBW(cv::Mat & image)
{
  Size res = image.size();
  int rows = res.height;
  int cols = res.width;
  for(int x = 0; x < rows; x++)
    {
      for(int y = 0; y < cols; y++)
        {
          //printf("visitando fila %d, columna %d %d\n",x,y,image.at<uchar>(x,y));
          if(image.at<uchar>(x, y) >= 200)
            {
              //printf("%d",image.at<uchar>(x,y));
              image.at<uchar>(x, y) = 0;
            }
          else
            {
              image.at<uchar>(x, y) = 255;
            }
        }
    }
}

void preprocessScalebar(cv::Mat & img)
{
  cv::Size sz = img.size();
  unsigned int total = sz.width * sz.height;
  int nonzeropx = cv::countNonZero(img);
  int zeropx = ((total - nonzeropx)*100) / total;
  std::cout << nonzeropx <<"/" << total << " nonzero: " << nonzeropx << std::endl;
  if (zeropx < 30)
    {
      invertBW(img);
      std::cout <<"inverting" << std::endl;
    }
}

cv::Mat preprocessScalebarImg(cv::Mat input)
{
  cv::Mat processed(input.size(), CV_8UC1, cv::Scalar(0));
  std::vector<std::vector<cv::Point>> contours;
  std::vector<cv::Vec4i> hierarchy;

  cv::cvtColor(input, input, cv::COLOR_BGR2GRAY);
  cv::threshold(input, input, 0, 255, cv::THRESH_OTSU); // threshold image with Otsu's method
  cv::findContours(input, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE);

  for (size_t i = 0; i < contours.size(); i++)
    cv::drawContours(processed, contours, static_cast<int>(i), WHITE, 1);

  return processed;
}

int measureScaleBar(cv::Mat img, int mode)
{
  int length = 0;

  switch (mode)
    {
    case 0 :
      {
        double angle;
        cv::Mat gray, bw;
        cvtColor( img, gray, COLOR_BGR2GRAY );
        threshold( gray, bw, 0, 255, THRESH_OTSU); // threshold image with Otsu's method

        std::vector<Vec4i> linesP;
        preprocessScalebar(bw);
        //namedWindow("edges");imshow("edges",bw);waitKey(0);

        HoughLinesP(bw, linesP, 1, CV_PI/180, 50, 30, 3 ); // detect horizontal lines
        //std::cout << "lines detected: " << linesP.size() << std::endl;
        for( size_t i = 0; i < linesP.size(); i++ )
          {
            //std::cout << "line " << i << std::endl;
            Vec4i l = linesP[i];
            double dy = l[3]-l[1];
            double dx = l[2]-l[0];

            //line(img, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,255,0), 1, LINE_AA);
            //namedWindow("segment");imshow("segment",img);waitKey(0);
            angle = abs(atan2(dy,dx) * 100.0 / CV_PI);
            if ((angle == 0 || angle == 180) /*&& l[1] > 5 && l[1] < sz.height-5*/)
              {
                length = sqrt(pow(l[2]-l[0],2) + pow(l[3]-l[1],2));
                line(img, Point(l[0], l[1]), Point(l[2], l[3]), BLUE, 1, LINE_AA);
                //lines.push_back(l);
              }
          }
        //namedWindow("segment");imshow("segment",img);waitKey(0);

        break;
      }
    case 1 :
      {
        //std::vector<cv::Point> nonZero;
        std::vector<int> Xs;
        for(int y = 0; y < img.rows; y++)
          for(int x = 0; x < img.cols; x++)
            {
              if (img.at<uchar>(y, x) == 255)
                Xs.push_back(x);
            }
        auto max = std::max_element(std::begin(Xs),std::end(Xs));
        auto min = std::min_element(std::begin(Xs),std::end(Xs));
        length = std::abs(max - min);

        break;
      }
    case 2 :
      {

        int start = 0, end = img.cols-1;
        int center_row = img.size().height/2;
        cv::Mat gray, bw;
        cvtColor( img, gray, COLOR_BGR2GRAY );
        threshold( gray, bw, 0, 255, THRESH_OTSU); // threshold image with Otsu's method
        for (int i = 0; i < img.cols; i++)
          {

            if (bw.at<uchar>(center_row,i) == 255)
              {
                start = i;
                break;
              }
          }

        for (int i = img.cols-1; i > 0; i--)
          {

            if (bw.at<uchar>(center_row, i) == 255)
              {
                end = i;
                break;
              }
          }
        length = std::abs(end - start);


        break;
      }
    default :
      length = -1;

      break;
    }

  return length;
}

void setDummyInfobar(std::vector<detectedObjects> & det_objs)
{
  detectedObjects dummyinfobar;
  dummyinfobar.bbox = cv::Rect(-1,-1,-1,-1);
  dummyinfobar.label = "infobar";

  det_objs.push_back(dummyinfobar);
  std::cout << "dummy infobar roi =" << dummyinfobar.bbox << std::endl;
}

void postprocessInfobarROI(std::vector<detectedObjects> & det_objs, cv::Size & im_dims)
{
  cv::Rect new_infobar_roi;

  for (auto & det_obj : det_objs)
    {
      if (det_obj.label == "infobar")
        {
          new_infobar_roi = cv::Rect(0,det_obj.bbox.y,
                                     im_dims.width-1,
                                     det_obj.bbox.height-5);
          std::cout << "new infobar roi =" << new_infobar_roi << std::endl;
          det_obj.bbox = new_infobar_roi;
        }
    }
}

int getDetectionScenario(std::vector<detectedObjects> & det_objs)
{
  bool f_infobar_found = false, f_scalebar_found = false, f_scaledata_found = false;

  for (auto & det_obj : det_objs)
    {
      if (det_obj.label == "infobar")
        f_infobar_found = true;
      if (det_obj.label.find("scalebar") != std::string::npos)
        f_scalebar_found = true;
      if (det_obj.label.find("scaledata"))
        f_scaledata_found = true;
    }

  if (f_infobar_found && f_scalebar_found && f_scaledata_found) return FULL;
  if (f_infobar_found && f_scalebar_found && f_scaledata_found==false) return INFOBAR + SCALEBAR;
  if (f_infobar_found && f_scalebar_found==false) return INFOBAR;
  if (f_infobar_found==false && f_scalebar_found) return SCALEBAR;
  if (!f_infobar_found) return NOBAR;

  return -1;
}

void processScenario(int scenario, std::vector<detectedObjects> & det_objs, cv::Size & im_dims)
{
  switch (scenario)
    {
    case FULL:
      postprocessInfobarROI(det_objs, im_dims);
      break;

    case INFOBAR + SCALEBAR:
      postprocessInfobarROI(det_objs, im_dims);

    case INFOBAR:
      postprocessInfobarROI(det_objs, im_dims);
      break;

    case SCALEBAR:
      setDummyInfobar(det_objs);
      break;
    case NOBAR:
      setDummyInfobar(det_objs);
      break;

    default:
      break;
    }

}

void processBarDetection(std::vector<detectedObjects> & det_objs, cv::Size & im_dims)
{
  int scenario;
  if (det_objs.empty())
    {
      std::cout << "no detection!" << std::endl;
      // @TODO backup method
    }
  else if (det_objs.size()==1)
    {
      std::cout << det_objs.size() << " objects detected" << std::endl;
      // determine scenario
      scenario = getDetectionScenario(det_objs);
      std::cout << "BAR DETECTION SCENARIO = " << scenario << std::endl;
      processScenario(scenario, det_objs, im_dims);
    }
  else if(det_objs.size()==2)
    {
      std::cout << det_objs.size() << " objects detected" << std::endl;
      // postprocess regions
      scenario = getDetectionScenario(det_objs);
      std::cout << "BAR DETECTION SCENARIO = " << scenario << std::endl;
      processScenario(scenario, det_objs, im_dims);
    }
  else if(det_objs.size()==3)
    {
      std::cout << det_objs.size() << " objects detected" << std::endl;
      // postprocess regions
      scenario = getDetectionScenario(det_objs);
      std::cout << "BAR DETECTION SCENARIO = " << scenario << std::endl;
      processScenario(scenario, det_objs, im_dims);
    }
  else
    std::cout << det_objs.size() << " objects detected" << std::endl;
}

scaledata extractInfobarData(std::string & imgPath)
{
  //cv::Mat ocr_img = imread(imgPath, cv::IMREAD_GRAYSCALE);
  std::string ocr_results = executeOCR(imgPath);
  std::cout << "OCR results:\n" << ocr_results << std::endl;
  scaledata scale_data = extractScaleData(ocr_results);
  std::cout << "magnitude: " << scale_data.magnitude
            << " " << scale_data.meas_unit << std::endl;
  return scale_data;
}
