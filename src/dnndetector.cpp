/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#include "dnndetector.hpp"

float confThreshold, nmsThreshold;
std::vector<std::string> classes;
std::vector<detectedObjects> detected;

std::vector<detectedObjects> dnndetect(cv::Mat & img, const std::string & model, const std::string & config, const std::string & classes_file)
{
  detected.clear();
  classes.clear();

  confThreshold = 0.85;
  nmsThreshold = 0.4;

  float scale = 0.00392;
  Scalar mean = Scalar(1,1,1);
  bool swapRB = true;
  int inpWidth = 512;
  int inpHeight = 512;
  int backend = 0;
  std::string classentry;
  std::vector<cv::Mat> outs;


  std::ifstream ifs(classes_file.c_str());
  if (!ifs.is_open())
    CV_Error(Error::StsError, "File " + classes_file + " not found");


  while (std::getline(ifs, classentry))
      classes.push_back(classentry);

  cv::dnn::Net net = cv::dnn::readNet(model, config);
  net.setPreferableBackend(backend);
  net.setPreferableTarget(0);

  std::vector<String> outNames = net.getUnconnectedOutLayersNames();

  preprocess(img, net, Size(inpWidth, inpHeight), scale, mean, swapRB);

  net.forward(outs, outNames);
  postprocess(img, outs, net, backend);

  return detected;
}

inline void preprocess(const Mat& frame, cv::dnn::Net& net, Size inpSize, float scale,
                       const Scalar& mean, bool swapRB)
{
  using namespace cv;
  static cv::Mat blob;
  if (inpSize.width <= 0) inpSize.width = frame.cols;
  if (inpSize.height <= 0) inpSize.height = frame.rows;
  cv::dnn::blobFromImage(frame, blob, 1.0, inpSize, cv::Scalar(), swapRB, false, CV_8U);

  net.setInput(blob, "", scale, mean);
  if (net.getLayer(0)->outputNameToIndex("im_info") != -1)
    {
      resize(frame, frame, inpSize);
      cv::Mat imInfo = (Mat_<float>(1, 3) << inpSize.height, inpSize.width, 1.6f);
      net.setInput(imInfo, "im_info");
    }
}

void postprocess(Mat& frame, const std::vector<Mat>& outs, cv::dnn::Net& net, int backend)
{
  static std::vector<int> outLayers = net.getUnconnectedOutLayers();
  static std::string outLayerType = net.getLayer(outLayers[0])->type;

  std::vector<int> classIds;
  std::vector<float> confidences;
  std::vector<Rect> boxes;
  if (outLayerType == "DetectionOutput")
    {
      // [batchId, classId, confidence, left, top, right, bottom]
      CV_Assert(outs.size() > 0);
      for (size_t k = 0; k < outs.size(); k++)
        {
          float* data = (float*)outs[k].data;
          for (size_t i = 0; i < outs[k].total(); i += 7)
            {
              float confidence = data[i + 2];
              if (confidence > confThreshold)
                {
                  int left   = (int)data[i + 3];
                  int top    = (int)data[i + 4];
                  int right  = (int)data[i + 5];
                  int bottom = (int)data[i + 6];
                  int width  = right - left + 1;
                  int height = bottom - top + 1;
                  if (width <= 2 || height <= 2)
                    {
                      left   = (int)(data[i + 3] * frame.cols);
                      top    = (int)(data[i + 4] * frame.rows);
                      right  = (int)(data[i + 5] * frame.cols);
                      bottom = (int)(data[i + 6] * frame.rows);
                      width  = right - left + 1;
                      height = bottom - top + 1;
                    }
                  classIds.push_back((int)(data[i + 1]) - 1);  // Skip 0th background class id.
                  boxes.push_back(Rect(left, top, width, height));
                  confidences.push_back(confidence);
                }
            }
        }
    }
  else if (outLayerType == "Region")
    {
      for (size_t i = 0; i < outs.size(); ++i)
        {
          float* data = (float*)outs[i].data;
          for (int j = 0; j < outs[i].rows; ++j, data += outs[i].cols)
            {
              Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
              Point classIdPoint;
              double confidence;
              minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
              if (confidence > confThreshold)
                {
                  int centerX = (int)(data[0] * frame.cols);
                  int centerY = (int)(data[1] * frame.rows);
                  int width = (int)(data[2] * frame.cols);
                  int height = (int)(data[3] * frame.rows);
                  int left = centerX - width / 2;
                  int top = centerY - height / 2;

                  classIds.push_back(classIdPoint.x);
                  confidences.push_back((float)confidence);
                  boxes.push_back(Rect(left, top, width, height));
                }
            }
        }
    }
  else
    CV_Error(Error::StsNotImplemented, "Unknown output layer type: " + outLayerType);

  if (outLayers.size() > 1 || (outLayerType == "Region" && backend != cv::dnn::DNN_BACKEND_OPENCV))
    {
      std::map<int, std::vector<size_t> > class2indices;
      for (size_t i = 0; i < classIds.size(); i++)
        {
          if (confidences[i] >= confThreshold)
            {
              class2indices[classIds[i]].push_back(i);
            }
        }
      std::vector<Rect> nmsBoxes;
      std::vector<float> nmsConfidences;
      std::vector<int> nmsClassIds;
      for (std::map<int, std::vector<size_t> >::iterator it = class2indices.begin(); it != class2indices.end(); ++it)
        {
          std::vector<Rect> localBoxes;
          std::vector<float> localConfidences;
          std::vector<size_t> classIndices = it->second;
          for (size_t i = 0; i < classIndices.size(); i++)
            {
              localBoxes.push_back(boxes[classIndices[i]]);
              localConfidences.push_back(confidences[classIndices[i]]);
            }
          std::vector<int> nmsIndices;
          cv::dnn::NMSBoxes(localBoxes, localConfidences, confThreshold, nmsThreshold, nmsIndices);
          for (size_t i = 0; i < nmsIndices.size(); i++)
            {
              size_t idx = nmsIndices[i];
              nmsBoxes.push_back(localBoxes[idx]);
              nmsConfidences.push_back(localConfidences[idx]);
              nmsClassIds.push_back(it->first);
            }
        }
      boxes = nmsBoxes;
      classIds = nmsClassIds;
      confidences = nmsConfidences;
    }

  for (size_t idx = 0; idx < boxes.size(); ++idx)
    {
      Rect box = boxes[idx];
      detectedObjects dob;
      dob.bbox = cv::Rect(Point(box.x,box.y),Point(box.x + box.width, box.y + box.height));
      dob.label = classes[classIds[idx]];
      dob.confidence = confidences[idx];
      detected.push_back(dob);
    }
}
