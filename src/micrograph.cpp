/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

// micrograph.cpp -- implementing the Micrograph class

#include "micrograph.hpp"

Micrograph::Micrograph( const std::string fname )
{
  m_filename = fname;
  loadMicrograph();
}

void Micrograph::setOutputDir(std::string opd)
{
  m_outputdir = opd;
}

void Micrograph::setBasename(std::string bn)
{
  m_basename = bn;
}

bool Micrograph::loadMicrograph()
{
  m_original_micrograph = cv::imread(m_filename, cv::IMREAD_COLOR);
  m_output_micrograph = cv::imread(m_filename, cv::IMREAD_COLOR);
  if (m_original_micrograph.empty()) return false;
  else return true;
}

void Micrograph::saveData()
{
  if (!m_original_micrograph.empty())
    imwrite(m_outputdir + PATH_SEPARATOR + "original.jpg", m_original_micrograph); // save original image
  if (!m_scene_roi.empty())
    imwrite(m_outputdir + PATH_SEPARATOR + "scene.jpg", cv::Mat(m_original_micrograph, m_scene_roi)); // save scene image
  if (!m_infobar_roi.empty())
    {
      std::cout << "saving infobar image with roi = " << m_infobar_roi
                << "with path " << m_outputdir + "_infobar.jpg"
                                                                << "on original image size " << m_original_micrograph.size() << std::endl;
      imwrite(m_outputdir + PATH_SEPARATOR + "infobar.jpg", cv::Mat(m_original_micrograph, m_infobar_roi)); // save infobar image
    }
  if (!m_scalebar_roi.empty())
    {
      std::cout << "saving scalebar image with roi = " << m_scalebar_roi
                << "with path " << m_outputdir + "_scalebar.jpg"
                                                                << "on original image size " << m_original_micrograph.size() << std::endl;
      imwrite(m_outputdir + PATH_SEPARATOR + "scalebar.jpg",cv::Mat(m_original_micrograph, m_scalebar_roi)) ; // save scalebar image
    }
  if (!m_output_micrograph.empty())
    imwrite(m_outputdir + PATH_SEPARATOR + "output.jpg", m_output_micrograph); // save original image
}

void Micrograph::saveResults()
{
  if (!m_reportStr.empty())
    {
      std::ofstream resultsFile;
      resultsFile.open(m_outputdir + PATH_SEPARATOR + "results.txt");
      resultsFile << m_reportStr;
      resultsFile.close();
    }
}

std::string Micrograph::saveImgForOCR(std::string img_format, int mode)
{
  std::string OCR_SFX = "ocr" + img_format;
  std::string img_fullpath;
  if (!m_infobar_roi.empty())
    {
      cv::Mat ocr_img, rgb_img, ocr_inv_img;
      img_fullpath = m_outputdir + PATH_SEPARATOR + OCR_SFX;
      rgb_img = cv::Mat(m_original_micrograph, m_ocr_roi);

      if (rgb_img.size().width <= 40)
        {
          cv::Mat resz_img(256, 5116, CV_8UC3);
          cv::resize(rgb_img, resz_img, resz_img.size(), 0, 0, cv::INTER_CUBIC);
          cv::cvtColor(resz_img, ocr_img, cv::COLOR_BGR2GRAY);
        }
      else
        cv::cvtColor(rgb_img, ocr_img, cv::COLOR_BGR2GRAY);

      if (mode == 1)
        cv::bitwise_not(ocr_img, ocr_img);

      imwrite(img_fullpath, ocr_img);

      return img_fullpath;
    }
  else
    return "";
}

void Micrograph::setMainROIs(std::vector<detectedObjects> det_objs)
{
  if(det_objs.size() == 0)
    {
      std::cout << "setting rois with dummy, no objs detected " << std::endl;
      cv::Size sz = m_original_micrograph.size();
      m_scene_roi = cv::Rect(0,0,sz.width-1,sz.height-1);
    }

  for (auto & det_obj : det_objs)
    {
      if (det_obj.label == "infobar")
        {
          if (det_obj.bbox.x == -1 || det_obj.bbox.x < 0)
            {
              cv::Size sz = m_original_micrograph.size();
              m_scene_roi = cv::Rect(0,0,sz.width-1,sz.height-1);
              m_infobar_roi = m_scene_roi;
              std::cout << "setting rois with dummy "
                        << "scene " << m_scene_roi
                        << "infobar " << m_infobar_roi << std::endl;
            }
          else
            {
              m_infobar_roi = det_obj.bbox;
              m_scene_roi = cv::Rect(0,0,m_infobar_roi.width,m_infobar_roi.y);
              std::cout << "setting rois without dummies "
                        << "scene " << m_scene_roi
                        << "infobar " << m_infobar_roi << std::endl;
            }
        }
      if (det_obj.label.find("scalebar") != std::string::npos)
        {
          m_scalebar_roi = det_obj.bbox;
        }
      if (det_obj.label.find("scaledata") != std::string::npos)
        {
          std::cout << "OCR ROI = " << det_obj.bbox << std::endl;
          m_ocr_roi = det_obj.bbox;
        }
    }
}

void Micrograph::addNP(cv::Rect b, std::string t, float c)
{
  cv::Rect new_location = fitCropRegion(b, getSceneImg().size());

  Nanoparticle np(t, new_location, c, getSceneImg());
  m_nanoparticles.push_back(np);
}

void Micrograph::addScaleInfo(cv::Rect b, std::string t)
{
  if (t == "bar") m_scalebar_roi = b;
  if (t == "data") m_scaledata_roi = b;
}

int Micrograph::doScalebarDetection()
{
  scaledata scd;
  // scalebar found
  if (!m_scalebar_roi.empty())
    {
      cv::Mat img_scalebar = cv::Mat(m_original_micrograph, m_scalebar_roi);

      if (m_scalebar_type == "scalebar-I-Beam")
        {
          m_scalebar_length = measureScaleBar(img_scalebar, 0);
        }
      if (m_scalebar_type == "scalebar-simple")
        {
          cv::Mat img_preproc = preprocessScalebarImg(img_scalebar);
          m_scalebar_length = measureScaleBar(img_preproc, 1);
        }
      if (m_scalebar_type == "scalebar-pillars")
        {

          m_scalebar_length = measureScaleBar(img_scalebar, 2);
        }

      if (m_scalebar_length <= 0) m_scalebar_measured = false;
      else m_scalebar_measured = true;

      if (m_scaledata.magnitude > 0) m_magnitude_found = true;
      else m_magnitude_found = false;

      if (m_scaledata.meas_unit != "Null") m_meas_unit_found = true;
      else m_meas_unit_found = false;



    }
  else // if no scalebar is detected, set data to -1 and return error
    {
      std::cout << "no scalebar detected" << std::endl;
      m_scalebar_length = -1;
      return -1;
    }

  return 0;
}

cv::Mat img_ctrs;
int Micrograph::doMorph()
{
try{

  std::string reportStr = " ***** Morphological Characterization Results ***** \n";

  if (m_nanoparticles.empty()) return -1;
  else
    {
      m_np_area = 0;
      m_scene_area = getSceneImg().rows * getSceneImg().cols;
      //cv::Mat scene_img = cv::Mat(m_output_micrograph, m_scene_roi).clone();
      cv::Mat scene_img;

      cv::Mat np_img, np_img_colour;
      cv::Mat clustered_img;

      //unsigned int total_nps = m_nanoparticles.size();
      unsigned short counter = 0;
      std::vector<std::vector<cv::Point>> raw_contours, contours;
      std::vector<cv::Point> majorAxis, minorAxis;

      cv::Point centroid, img_center;
      std::vector<cv::Point> np_contours;
      int contour_idx;
      bool contour_not_found = true;




      Nanoparticle np;
      for (size_t n = 0; n < m_nanoparticles.size(); n++)// run process for each NP
        {

          np = m_nanoparticles.at(n);


          scene_img = cv::Mat(m_output_micrograph, m_scene_roi).clone();


          counter++;
          contour_not_found = true;

          std::cout << "Running morphological characterization for " << np.type()
                    << " at " << np.location() << std::endl;

          cv::Rect new_region = fitCropRegion(np.location(), scene_img.size());

          np.setLocation(new_region); // update np bbox
          np.setNp_img(cv::Mat(scene_img,np.location()));

          np_img = np.np_img();
          cv::imwrite(m_outputdir + PATH_SEPARATOR + "NPs" + PATH_SEPARATOR + "postcrop_" + std::to_string(counter) + ".jpg", np_img); // save image
          np_img_colour = cv::Mat(scene_img, new_region);
          img_center = cv::Point(np_img.size().width/2,np_img.size().height/2);

          cv::cvtColor(np_img, np_img, cv::COLOR_BGR2GRAY); // convert to grayscale



          if (np.type() == "cube")
            clustered_img = bykmeans(np_img, 3);
          if(np.type() == "quasi-sphere")
            clustered_img = bykmeans(np_img, 2);
          /*else
            clustered_img = bykmeans(np_img, 2);*/

          cv::GaussianBlur(clustered_img, np_img, cv::Size(5, 5), 0);
          cv::GaussianBlur(np_img, np_img, cv::Size(3, 3), 0);

          cv::threshold(np_img, np_img, 0, 255, cv::THRESH_OTSU); // and binarize with Otsu

          imwrite(m_outputdir + PATH_SEPARATOR + "NPs" + PATH_SEPARATOR + "postbw_" + std::to_string(counter) + ".jpg", np_img); // save image


          std::vector<cv::Vec4i> hierarchy;
          cv::findContours(np_img, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE);//CHAIN_APPROX_SIMPLE

          if (contours.empty()) break;
          std::cout << "Contours found" << std::endl;
          //



          for (size_t i = 0; i < contours.size(); i++)
            {
              centroid = getCenter(contours[i]);
              double point_in = cv::pointPolygonTest(contours[i], img_center, false);

              if (point_in > 0)
                {
                  np.setCenter(centroid);
                  np_contours =  contours[i];
                  contour_idx = i;
                  contour_not_found = false;
            //
                  break;
                }
            }
          //

          if (contour_not_found)
            {

            m_nanoparticles.at(n).setArea(0);
            m_nanoparticles.at(n).setMajorAxis_length(0);
            m_nanoparticles.at(n).setMinorAxis_length(0);
            reportStr += "** NP #" + std::to_string(counter) + "\n";
            reportStr += "Type: " + np.type() + "\n";
            reportStr += "Unable to run morphological characterization for this particle\n";
            m_nanoparticles.at(n).setReportStr(reportStr);

            cv::imwrite(m_outputdir + PATH_SEPARATOR + "NPs" + PATH_SEPARATOR + "np_" + std::to_string(counter) + ".jpg", np_img_colour); // save image
            m_nanoparticles.at(n).setImagePath(m_outputdir + PATH_SEPARATOR + "NPs" + PATH_SEPARATOR + "np_" + std::to_string(counter) + ".jpg");

            contours.clear();
            reportStr.clear();
            continue;
            }
          //else
            //{

              double area = contourArea(np_contours);
              np_img_colour = np.np_img().clone();


                  std::cout << "contour size = " << np_contours.size()
                            << " area = " << area << std::endl;

                  // centroid
                  np.setReal_center(cv::Point(new_region.x+np.center().x, new_region.y+np.center().y));
                  m_nanoparticles.at(n).setReal_center(cv::Point(new_region.x+np.center().x, new_region.y+np.center().y));

                  // area
                  np.setArea(contourArea(np_contours));
                  np.setContours(np_contours);
                  m_nanoparticles.at(n).setArea(contourArea(np_contours));
                  m_np_area += contourArea(np_contours);

                  // major axis
                  majorAxis = findMajorAxis(np_contours);
                  np.setP1_majAx(majorAxis.at(0));
                  np.setP2_majAx(majorAxis.at(1));
                  np.setMajorAxis_length(sqrt(pow(np.p1_majAx().x - np.p2_majAx().x,2)
                                              + pow(np.p1_majAx().y - np.p2_majAx().y,2)));
                  m_nanoparticles.at(n).setMajorAxis_length(sqrt(pow(np.p1_majAx().x - np.p2_majAx().x,2)
                                                                 + pow(np.p1_majAx().y - np.p2_majAx().y,2)));


                  // minor axis
                  minorAxis = findMinorAxis(np_contours, majorAxis);
                  np.setP1_minAx(minorAxis.at(0));
                  np.setP2_minAx(minorAxis.at(1));
                  np.setMinorAxis_length(sqrt(pow(np.p1_minAx().x - np.p2_minAx().x,2)
                                              + pow(np.p1_minAx().y - np.p2_minAx().y,2)));
                  m_nanoparticles.at(n).setMinorAxis_length(sqrt(pow(np.p1_minAx().x - np.p2_minAx().x,2)
                                                                 + pow(np.p1_minAx().y - np.p2_minAx().y,2)));

                  if (m_scalebar_length > 0)
                    {
                      if (np.majorAxis_length() >= m_scalebar_length*0.2 && np.majorAxis_length() <= m_scalebar_length*3)
                        np.setValid_size(true);
                      else
                        np.setValid_size(false);
                    }

                  // draw axes and center
                  cv::drawContours(np_img_colour, contours, static_cast<int>(contour_idx), BLUE, 1);


                  cv::circle(getSceneImg(), np.real_center(), 1, MAGENTA, 2);
                  //cv::circle(np_img_colour, cv::Point(np_img.size().width/2,np_img.size().height/2), 1, GREEN, 1); // image center
                  cv::line(np_img_colour, majorAxis.at(1), majorAxis.at(0), YELLOW, 1, cv::LINE_AA);
                  cv::line(np_img_colour, minorAxis.at(1), minorAxis.at(0), CYAN, 1, cv::LINE_AA);

                  // save NP images
                  cv::imwrite(m_outputdir + PATH_SEPARATOR + "NPs" + PATH_SEPARATOR + "np_" + std::to_string(counter) + ".jpg", np_img_colour); // save image
                  cv::imwrite(m_outputdir + PATH_SEPARATOR + "NPs" + PATH_SEPARATOR + "cl_" + std::to_string(counter) + ".jpg", clustered_img);
                  m_nanoparticles.at(n).setImagePath(m_outputdir + PATH_SEPARATOR + "NPs" + PATH_SEPARATOR + "np_" + std::to_string(counter) + ".jpg");

          if (np.contours().empty()) break;

          reportStr += "** NP #" + std::to_string(counter) + "\n";
          reportStr += "Type: " + np.type() + "\n";
          reportStr += "Center at " + std::to_string(np.center().x) + "," + std::to_string(np.center().y) + "\n";
          reportStr += "Area: " + std::to_string(np.area()) +" square pixels \n";
          reportStr += "Major Axis from: (" + std::to_string(np.p1_majAx().x) + "," + std::to_string(np.p1_majAx().y)
              + ") to (" + std::to_string(np.p2_majAx().x) + "," + std::to_string(np.p2_majAx().y) + ") \n";
          reportStr += "Major Axis length: " + std::to_string(np.majorAxis_length()) + " pixels ~ "
                                                                                + pxToMetricStr(np.majorAxis_length()) + "\n";;
          reportStr += "Minor Axis from: (" + std::to_string(np.p1_minAx().x) + "," + std::to_string(np.p1_minAx().y)
              + ") to (" + std::to_string(np.p2_minAx().x) + "," + std::to_string(np.p2_minAx().y) + ") \n";
          reportStr += "Minor Axis length: " + std::to_string(np.minorAxis_length()) + " pixels ~ "
                                                                                + pxToMetricStr(np.minorAxis_length()) + "\n";



          //



          contours.clear();

          //np.setReportStr(reportStr);
          m_nanoparticles.at(n).setReportStr(reportStr);
          reportStr.clear();
          majorAxis.clear();
          //scene_img.release();
          //np.np_img().release();
          //contour_idx = -1;



        }// for NPs


      generateGlobalReport();
      doStatistics();
      //reportStr += " ***** /Morphological Characterization Results ***** \n";
      std::cout << reportStr << std::endl;

    }// else
}catch(const std::exception &e){std::cout << e.what();}
  return 0;
}//doMorph



int Micrograph::doStatistics()
{
  std::string reportStr;
  if (m_nanoparticles.size() > 1)
    {
      unsigned int distance = 0;
      unsigned int global_distance = 0;
      float major_axis = 0;
      float minor_axis = 0;
      float area = 0;
      for(size_t i = 0; i < m_nanoparticles.size(); i++)
        {
          for(size_t j = 0; j < m_nanoparticles.size(); j++)
            {
              if (m_nanoparticles[i].real_center() != m_nanoparticles[j].real_center())
                {
                  distance += std::sqrt(pow(m_nanoparticles[i].real_center().x - m_nanoparticles[j].real_center().x,2)
                                        + pow(m_nanoparticles[i].real_center().y - m_nanoparticles[j].real_center().y,2));
                }
            }
          m_nanoparticles[i].setMean_center_distance(distance/m_nanoparticles.size());
          global_distance += m_nanoparticles[i].mean_center_distance();

          major_axis += m_nanoparticles.at(i).majorAxis_length();
          minor_axis += m_nanoparticles.at(i).minorAxis_length();
          area += m_nanoparticles.at(i).area();

          distance = 0;
          reportStr += "Mean distance between centers: " + std::to_string(m_nanoparticles[i].mean_center_distance())
              + " px ~ " + pxToMetricStr(m_nanoparticles[i].mean_center_distance())+"\n";
          m_nanoparticles[i].setReportStr(reportStr);
          reportStr.clear();
        }


      global_distance = global_distance / m_nanoparticles.size();
      m_avg_np_major_axis = major_axis / m_nanoparticles.size();
      m_avg_np_minor_axis = minor_axis / m_nanoparticles.size();
      m_avg_area = area / m_nanoparticles.size();

      m_reportStr += "Global Mean distance between centers: " + std::to_string(global_distance)
          + " px ~ " + pxToMetricStr(global_distance) +"\n";
      m_reportStr += "Average major axis length: " + std::to_string(m_avg_np_major_axis)
          + " px ~ " + pxToMetricStr(m_avg_np_major_axis) +"\n";
      m_reportStr += "Average minor axis length: " + std::to_string(m_avg_np_minor_axis)
          + " px ~ " + pxToMetricStr(m_avg_np_minor_axis) +"\n";
      m_reportStr += "Average NP area: " + std::to_string(m_avg_area)
          + " square pixels\n";
    }
  else
    return -1;

  return 0;
}

std::string Micrograph::outputDir()
{
  return m_outputdir;
}

cv::Mat Micrograph::getOriginalImg()
{
  return m_original_micrograph;
}

cv::Mat Micrograph::getSceneImg()
{
  return cv::Mat(m_output_micrograph, m_scene_roi);
}

cv::Mat Micrograph::getInfobarImg()
{
  return cv::Mat(m_original_micrograph, m_infobar_roi);
}

cv::Mat Micrograph::getScalebarImg()
{
  return cv::Mat(m_original_micrograph, m_scalebar_roi);
}

cv::Mat Micrograph::getScaledataImg()
{
  cv::Mat infobar = getInfobarImg();
  return cv::Mat(infobar, m_scaledata_roi);
}

cv::Rect Micrograph::getInfobarROI()
{
  return m_infobar_roi;
}

unsigned Micrograph::getNPNumber()
{
  return m_nanoparticles.size();
}

float Micrograph::pxToMetric(int px_len)
{
  if (m_scalebar_length > 0)
    {
      float num = (float) (px_len * m_scaledata.magnitude);
      float metric_len = (float) num / m_scalebar_length;
      return metric_len;
    }
  else return 0;
}

std::string Micrograph::pxToMetricStr(int px_len)
{
  std::string metric_str = "";
  float metric = 0;

  if (m_scaledata.magnitude > 0 && m_scalebar_measured == true)
    {
      metric = pxToMetric(px_len);
    }


  if (metric == 0)
    metric_str = "N/A";
  else
    metric_str = std::to_string(metric) + " " + m_scaledata.meas_unit;

  return metric_str;
}

std::string Micrograph::getFileName()
{
  return m_filename;
}

void Micrograph::setScaledata(scaledata scd)
{
  m_scaledata = scd;
}

void Micrograph::setScalebarType( std::string type)
{
  m_scalebar_type = type;
}

std::string Micrograph::getReportStr()
{
  return m_reportStr;
}

std::vector<std::string> Micrograph::getReportList()
{
  for (auto & np : m_nanoparticles)
    {
      m_report_list.push_back(np.reportStr());
    }
  return m_report_list;
}

void Micrograph::generateGlobalReport()
{
  int nc=0, qs=0;

  m_reportStr += "RESULTS FOR FILE: " + m_filename + "\n";
  if (m_scalebar_measured)
    {
      if (m_magnitude_found && m_meas_unit_found)
        {
          m_reportStr += "Scalebar type: " + m_scalebar_type + "\n";
          m_reportStr += "Reference length (pixels): " + std::to_string(m_scalebar_length) + "\n";
          m_reportStr += "Reference length (metric): " + std::to_string(m_scaledata.magnitude) + " " + m_scaledata.meas_unit + "\n";
          m_reportStr += "1 pixel represents approximately  " + pxToMetricStr(1) + "\n";
        }
      else
        {
          m_reportStr += "Unable to extract scaledata. Measurements will be printed in pixels \n";
          m_reportStr += "Scalebar type: " + m_scalebar_type + "\n";
          m_reportStr += "Reference length (pixels): " + std::to_string(m_scalebar_length) + "\n";
        }
    }
  else
    m_reportStr += "Unable to detect any scalebar. Measurements will be printed in pixels \n";


  if (m_nanoparticles.size() > 0)
    {
      m_reportStr += "\nNANOPARTICLES:\n";
      m_reportStr += std::to_string(m_nanoparticles.size()) + " nanoparticles detected:" + "\n";
      for(auto & np : m_nanoparticles)
        {
          if (np.type() == "cube") nc++;
          if (np.type() == "quasi-sphere") qs++;
          //else unk++;
        }
      m_reportStr += std::to_string(nc) + " nanocubes." + "\n";
      m_reportStr += std::to_string(qs) + " quasi-spheres." + "\n";
      //m_reportStr += std::to_string(qs) + " unidentified." + "\n";
      m_reportStr += "Total area occupied by detected particles: " + std::to_string(m_np_area)
          + " square pixels,\n"
          + "representing approximately " + std::to_string((m_np_area * 100) / m_scene_area)
          + "% of the visible portion of the micrograph.\n";
    }
  else
    m_reportStr+= "Unable to detect any particles.\n";

}

std::vector<std::string> Micrograph::nanoparticlesPathList()
{
  std::vector<std::string> path_list;

  for (auto & np : m_nanoparticles)
    path_list.push_back(np.imagePath());

  return path_list;
}

void Micrograph::reserveNPMemory(int npcount)
{
  m_nanoparticles.reserve(npcount);
}
