/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#ifndef MICROGRAPH_H
#define MICROGRAPH_H

#include "common.hpp"
#include "nanoparticle.hpp"
#include "infobarprocessor.hpp"
#include "morphprocessor.hpp"
#include "clustering.hpp"

class Micrograph
{
private:
  std::string m_filename;
  std::string m_basename;
  std::string m_outputdir;
  std::string m_reportStr;
  std::vector<std::string> m_report_list;
  std::string m_scalebar_type;
  float m_np_area;
  float m_scene_area;
  float m_np_area_percentage;
  float m_avg_np_area;
  float m_avg_np_major_axis;
  float m_avg_np_minor_axis;
  float m_avg_area;
  unsigned m_scalebar_length;
  cv::Mat m_original_micrograph;
  cv::Mat m_output_micrograph;
  cv::Rect m_scene_roi;
  cv::Rect m_infobar_roi;
  cv::Rect m_scalebar_roi;
  cv::Rect m_scaledata_roi;
  cv::Rect m_ocr_roi;

  bool m_infobar_found;
  bool m_scalebar_found;
  bool m_scalebar_measured;
  bool m_magnitude_found;
  bool m_meas_unit_found;

  scaledata m_scaledata;
  std::vector<Nanoparticle> m_nanoparticles;

public:
  Micrograph(const std::string fname);
  // setters
  void setOutputDir( std::string );
  void setBasename( std::string );
  void setMainROIs( std::vector<detectedObjects> );
  void setScaledata( scaledata );
  void setScalebarType( std::string );
  // getters
  std::string outputDir();
  cv::Mat getOriginalImg( void );
  cv::Mat getSceneImg( void );
  cv::Mat getInfobarImg( void );
  cv::Mat getScalebarImg( void );
  cv::Mat getScaledataImg( void );
  cv::Rect getInfobarROI( void );
  unsigned getNPNumber( void );
  std::string getFileName( void );
  std::string getReportStr( void );
  std::vector<std::string> getReportList( void );
  std::vector<std::string> nanoparticlesPathList();

  bool loadMicrograph( void );
  void saveData( void );
  void saveResults( void );
  std::string saveImgForOCR( std::string, int );
  void addNP( cv::Rect, std::string, float );
  void addScaleInfo( cv::Rect, std::string );
  int doScalebarDetection( void );
  int doMorph( void );
  int doStatistics( void );
  float pxToMetric( int );
  std::string pxToMetricStr( int );
  void generateGlobalReport( void );
  void reserveNPMemory( int );
};
#endif
