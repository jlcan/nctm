/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#ifndef FILEOPERATIONS_H
#define FILEOPERATIONS_H
#include <cstdlib>
#include <vector>
#include <list>
#include <string>
#include <cmath>
#include "config.hpp"

// path, dir and filename manipulation
#include <QFileInfo>
#include <QDir>
#include <QString>

// PDF report printing
#include <QPrinter>
#include <QPainter>
#include <QFont>
#include <QRect>
#include <QDebug>
#include <QImageReader>
#include <QImage>

const std::list<std::string> valid_image_types = {".tif",".TIF",".tiff",".TIFF",".jpg",".JPG",".jpeg",".JPEG",".png",".PNG"};

QString getHomeDir();
std::string getConfigDir();
void setupLogs();
std::string createDirectory(std::string &);
std::string getBasename(std::string &);
void printReport(std::vector<std::string>, std::vector<std::string>, std::string, std::string &);
#endif
