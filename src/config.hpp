/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#ifndef NCTM_CONFIG_H
#define NCTM_CONFIG_H

#if defined(__linux__)
#define OS "Linux"
#define PATH_SEPARATOR "/"
#define HOME_VAR_NAME "HOME"
#define CONFIG_DIR_NAME ".config/NCTM"
#elif defined(_WIN32)
#define OS "Windows"
#define PATH_SEPARATOR "\\"
#define HOME_VAR_NAME "USERPROFILE"
#define CONFIG_DIR_NAME "\\AppData\\Local\\NCTM"
#define OCR_CMD "gocr.exe"
#define OCR_ARG " -u ? -C 0-9a-z. "
#endif

#define NCTM_OUTDIRNAME "NCTM"
#define LOG_FILE "output.log"
#define ERR_LOG_FILE "error.log"

// CCN model for scale data detection
#define CNN_BAR_MODEL "nctm-bar-detector-model.weights"
#define CNN_BAR_CONFIG "nctm-bar-detector.cfg"
#define CNN_BAR_CLASSES "nctm-bar-classes.names"

// CCN model for NP detection
#define CNN_MODEL "nctm-np-detector-model.weights"
#define CNN_CONFIG "nctm-np-detector.cfg"
#define CNN_CLASSES "nctm-np-classes.names"
#endif
