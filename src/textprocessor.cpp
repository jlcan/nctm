/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#include "textprocessor.hpp"

void sanitizeText(std::string & input)
{
  std::locale loc;
  std::regex regexp("\\s+");
  input = regex_replace(input, regexp, " ");
  for (std::string::size_type i=0; i<input.length(); ++i)
    input[i] = std::tolower(input[i], loc);
}

std::string executeOCR(std::string & imgPath)
{
  QString detected_text;
  QProcess ocr_proc;
  QStringList args;

  ocr_proc.setProgram(OCR_CMD);
  args.push_back(OCR_ARG);
  args.push_back(QString().fromStdString(imgPath));
  ocr_proc.setArguments(args);

  ocr_proc.start();
  ocr_proc.waitForFinished();
  detected_text = ocr_proc.readAllStandardOutput();

  if (detected_text.isEmpty()) return detected_text.toStdString();

  detected_text.remove("\r");
  detected_text.remove("\n");

  std::string ocr_output = detected_text.toStdString();
  sanitizeText(ocr_output);
  return ocr_output;
}

scaledata extractScaleData(std::string input)
{
  scaledata scd;
  std::regex regexp("([0-9]{1,4}\.*[0-9]*)\\s*(um|nm|mm)");
  std::smatch sm;
  regex_search(input, sm, regexp);
  /*for(int i = 0; i < sm.size(); i++)
    std::cout << "match: " << sm[i] << std::endl;*/

  if (sm.length() < 3)
    {
      scd.magnitude = -1;
      scd.meas_unit = "Null";
      return scd;
    }

  scd.magnitude = stoi(sm[1]);
  if (sm[2] == "mm")
    scd.meas_unit = "um";
  else
    scd.meas_unit = sm[2];

  return scd;
}
