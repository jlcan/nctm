/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#ifndef NCTM_INFOBARPROCESSOR_H
#define NCTM_INFOBARPROCESSOR_H

#include <cmath>
#include <fstream>
#include "common.hpp"
#include "textprocessor.hpp"

#define Y_AXIS 1
#define OCR_EXT ".pgm"
#define FALLBACK_MSG "::FALLBACK MODE::"

#define BAR_SFFX "_bar"
#define BAR_ROI_SFFX "_bar_roi"
#define CLEAN_SFFX "_clean"
#define SCALEBAR_SFFX "_scalebar"
#define OCR_SFFX "_scalebar_ocr"

enum InfobarDetectionScenarios {FULL = 0, INFOBAR = 1, SCALEBAR = 2, SCALEDATA = 3, NOBAR = 4};

// infobar processing
void setDummyInfobar(std::vector<detectedObjects> &);
void postprocessInfobarROI(std::vector<detectedObjects> &, cv::Size &);
int getDetectionScenario(std::vector<detectedObjects> &);
void processScenario(int scenario, std::vector<detectedObjects> &, cv::Size &);
void processBarDetection(std::vector<detectedObjects> &, cv::Size &);
cv::Mat preprocessScalebarImg(cv::Mat input);
int measureScaleBar(cv::Mat, int mode);
scaledata extractInfobarData(std::string &);
#endif

