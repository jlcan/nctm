/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#ifndef NCTM_TEXTPROCESSOR_H
#define NCTM_TEXTPROCESSOR_H
#include <string>
#include <regex>
#include "config.hpp"

#include <QProcess>
#include <QDebug>
#include <QString>
//#include <iostream>

//using namespace std;

struct scaledata
{
  int magnitude;
  std::string meas_unit;
};

void sanitizeText(std::string &);
std::string executeOCR(std::string &);
scaledata extractScaleData(std::string);
scaledata getScaleData(const std::string &);
#endif
