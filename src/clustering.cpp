/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#include "clustering.hpp"

cv::Mat bykmeans(cv::Mat img, int k)
{
  cv::Mat img_gray, labels, centers;
  cv::TermCriteria term_crit = cv::TermCriteria(TermCriteria::EPS+TermCriteria::COUNT, 100, 0.2);
  Mat samples(img.rows * img.cols, img.channels(), CV_32F);
  Mat out_img(img.size(), img.type());
  int attempts = 3;

  for (int y = 0; y < img.rows; y++)
    for (int x = 0; x < img.cols; x++)
      for (int z = 0; z < img.channels(); z++)
        samples.at<float>(y + x * img.rows, z) = img.at<uchar>(y, x);

  kmeans(samples, k, labels, term_crit, attempts, cv::KMEANS_PP_CENTERS, centers);

  for (int y = 0; y < img.rows; y++)
    for (int x = 0; x < img.cols; x++)
      {
        int cluster_idx = labels.at<int>(y + x * img.rows, 0);
        out_img.at<uchar>(y, x) = centers.at<float>(cluster_idx, 0);
      }

  return out_img;
}
