/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

// nanoparticle.cpp -- implementing the Nanoparticle class

#include "nanoparticle.hpp"

Nanoparticle::Nanoparticle()
{

}

Nanoparticle::Nanoparticle(std::string t, cv::Rect l, float c)
{
  m_type = t;
  m_location = l;
  m_confidence = c;
}

Nanoparticle::Nanoparticle(std::string t, cv::Rect l, float c, cv::Mat img)
{
  m_type = t;
  m_location = l;
  m_confidence = c;
  m_np_img = cv::Mat(img, l);
}

const std::string &Nanoparticle::type() const
{
  return m_type;
}

const cv::Rect &Nanoparticle::location() const
{
  return m_location;
}

void Nanoparticle::setCenter(cv::Point newCenter)
{
  m_center = newCenter;
}

cv::Point Nanoparticle::center() const
{
  return m_center;
}

cv::Point Nanoparticle::real_center() const
{
  return m_real_center;
}

void Nanoparticle::setReal_center(cv::Point newReal_center)
{
  m_real_center = newReal_center;
}

float Nanoparticle::mean_center_distance() const
{
  return m_mean_center_distance;
}

void Nanoparticle::setMean_center_distance(float newMean_center_distance)
{
  m_mean_center_distance = newMean_center_distance;
}

bool Nanoparticle::valid_size() const
{
  return m_valid_size;
}

unsigned int Nanoparticle::minorAxis_length() const
{
  return m_minorAxis_length;
}

void Nanoparticle::setMinorAxis_length(unsigned int newMinorAxis_length)
{
  m_minorAxis_length = newMinorAxis_length;
}

unsigned int Nanoparticle::majorAxis_length() const
{
  return m_majorAxis_length;
}

void Nanoparticle::setMajorAxis_length(unsigned int newMajorAxis_length)
{
  m_majorAxis_length = newMajorAxis_length;
}

cv::Point Nanoparticle::p1_majAx() const
{
  return m_p1_majAx;
}

void Nanoparticle::setP1_majAx(cv::Point newP1_majAx)
{
  m_p1_majAx = newP1_majAx;
}

cv::Point Nanoparticle::p2_majAx() const
{
  return m_p2_majAx;
}

void Nanoparticle::setP2_majAx(cv::Point newP2_majAx)
{
  m_p2_majAx = newP2_majAx;
}

cv::Point Nanoparticle::p1_minAx() const
{
  return m_p1_minAx;
}

void Nanoparticle::setP1_minAx(cv::Point newP1_minAx)
{
  m_p1_minAx = newP1_minAx;
}

cv::Point Nanoparticle::p2_minAx() const
{
  return m_p2_minAx;
}

void Nanoparticle::setP2_minAx(cv::Point newP2_minAx)
{
  m_p2_minAx = newP2_minAx;
}

void Nanoparticle::setValid_size(bool newValid_size)
{
  m_valid_size = newValid_size;
}

const unsigned &Nanoparticle::area() const
{
  return m_area;
}

void Nanoparticle::setArea(const unsigned &newArea)
{
  m_area = newArea;
}

void Nanoparticle::setContours(std::vector<cv::Point> newContours)
{
  m_contours = newContours;
}

std::vector<cv::Point> Nanoparticle::contours()
{
  return m_contours;
}

void Nanoparticle::setLocation(const cv::Rect &newLocation)
{
  m_location = newLocation;
}

cv::Mat Nanoparticle::np_img()
{
  return m_np_img;
}

void Nanoparticle::setNp_img(cv::Mat newNp_img)
{
  m_np_img = newNp_img;
}

void Nanoparticle::setReportStr(std::string istr)
{
  m_reportStr += istr;
}

std::string Nanoparticle::reportStr()
{
  return m_reportStr;
}

void Nanoparticle::setImagePath(std::string istr)
{
  m_imagepath += istr;
}

std::string Nanoparticle::imagePath()
{
  return m_imagepath;
}
