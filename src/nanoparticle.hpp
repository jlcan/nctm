/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#ifndef NANOPARTICLE_H
#define NANOPARTICLE_H

#include "common.hpp"

class Nanoparticle
{
public:
  Nanoparticle();
  Nanoparticle(std::string t, cv::Rect l, float c);
  Nanoparticle(std::string t, cv::Rect l, float c, cv::Mat img);

  void setType(const std::string &newType);
  const std::string &type() const;
  const cv::Rect &location() const;


  void setCenter(cv::Point newCenter);

  cv::Point center() const;

  cv::Point real_center() const;
  void setReal_center(cv::Point newReal_center);

  float mean_center_distance() const;
  void setMean_center_distance(float newMean_center_distance);

  void setValid_size(bool newValid_size);

  const unsigned &area() const;
  void setArea(const unsigned &newArea);

  bool valid_size() const;

  unsigned int minorAxis_length() const;
  void setMinorAxis_length(unsigned int newMinorAxis_length);

  unsigned int majorAxis_length() const;
  void setMajorAxis_length(unsigned int newMajorAxis_length);

  cv::Point p1_majAx() const;
  void setP1_majAx(cv::Point newP1_majAx);

  cv::Point p2_majAx() const;
  void setP2_majAx(cv::Point newP2_majAx);

  cv::Point p1_minAx() const;
  void setP1_minAx(cv::Point newP1_minAx);

  cv::Point p2_minAx() const;
  void setP2_minAx(cv::Point newP2_minAx);

  void setContours(std::vector<cv::Point> newContours);
  std::vector<cv::Point> contours();

  void setLocation(const cv::Rect &newLocation);

  cv::Mat np_img();
  void setNp_img(cv::Mat newNp_img);

  void setReportStr(std::string);
  std::string reportStr( void );

  void setImagePath(std::string);
  std::string imagePath( void );

private:
  cv::Mat m_np_img;
  std::string m_type;
  cv::Rect m_location;
  std::string m_reportStr;
  std::string m_imagepath;
  float m_confidence;
  unsigned m_area;
  double m_orientation;
  unsigned int m_majorAxis_length;
  unsigned int m_minorAxis_length;
  float m_mean_center_distance;
  bool m_valid_size;

  cv::Point m_center;
  cv::Point m_real_center;
  cv::Point m_p1_majAx, m_p2_majAx;
  cv::Point m_p1_minAx, m_p2_minAx;
  std::vector<cv::Point> m_contours;
};

#endif
