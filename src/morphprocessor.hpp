/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

// morphprocessor.hpp -- morphological characterization
#ifndef MORPH_PROC_H
#define MORPH_PROC_H
#include <cmath>

#include "common.hpp"

#define PADDING 5

cv::Rect fitCropRegion(cv::Rect, cv::Size);
cv::Rect increaseCropRegion(cv::Rect, cv::Size);
std::vector<cv::Point> findMajorAxis(std::vector<cv::Point>);
std::vector<cv::Point> findMinorAxis(std::vector<cv::Point>, std::vector<cv::Point>);
void drawAxis(cv::Mat&, cv::Point, cv::Point, cv::Scalar, const float);
double getOrientation(const std::vector<cv::Point> &, cv::Mat&);
cv::Point getCenter(const std::vector<cv::Point> &);
float distanceBetPoints(cv::Point &, cv::Point &);
#endif
