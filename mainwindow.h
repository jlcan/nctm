/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QMainWindow>
#include <QImage>

QT_BEGIN_NAMESPACE
class QAction;
class QLabel;
class QMenu;
class QScrollArea;
class QScrollBar;
class QProgressDialog;
QT_END_NAMESPACE

#include "nctmworker.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();
    bool loadFile(const QString &);

private slots:
    void open();
    void openDirectory();
    void normalSize();
    void fitToWindow();
    void nextImage();
    void prevImage();
    void clear();
    void about();

private:
    void createActions();
    void createMenus();
    void updateActions();
    void updateStatusLabel(QString, int);
    void setImage(const QImage &newImage);
    void scaleImage(double factor);
    void adjustScrollBar(QScrollBar *scrollBar, double factor);
    void updateFileList();
    void processMicrograph();
    void processDirectory();

    enum msgType  {CLEAR, LOADED, BUSY, DONE};
    int fileIdx;
    int maxFileListIdx;
    bool f_dirSelected;
    double scaleFactor;
    QString imagePath;
    QString dirPath;
    QString statusMsg;
    QStringList fileList;
    QImage image;
    QLabel *imageLabel;
    QLabel *statusLabel;
    QScrollArea *scrollArea;

    QAction *normalSizeAct;
    QAction *fitToWindowAct;
    QAction *nextImageAct;
    QAction *prevImageAct;
    QAction *processMicrographAct;
    QAction *processDirectoryAct;
    QAction *clearAct;
};


#endif
