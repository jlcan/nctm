/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#ifndef NCTMWORKER_H
#define NCTMWORKER_H

#include <QObject>
#include <QtConcurrent>
#include <QProgressDialog>

#include "src/nctm.hpp"

class NCTNMWorker : public QObject
{
  Q_OBJECT
public:
  explicit NCTNMWorker(QObject *parent = nullptr);
  void process_micrograph(QString &);
  void process_micrograph(QStringList &);

private:
  int run_singleFile(QString);
  int run_batch(QStringList);
  QFutureWatcher<void> ftWatcher;
};

#endif // NCTMWORKER_H
