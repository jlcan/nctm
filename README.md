## Introduction

Nanoparticle Characterization Tool for Micrographs (NCTM) is a program to perform morphological charactertization of nanoparticles in scanning electron microscopy (SEM) micrographs.

## Building

### Dependencies

* > C++ 11
* OpenCV 4.5.X (it should compile on 3.4 as well)
* Qt5 5.12
* gocr 0.49

## License
This program is licensed under the terms of the version 3 of the GNU General Public License. See the LICENSE.txt file for more details.