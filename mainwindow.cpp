/**********************************************************************
//Copyright (c) 2021 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#include <QtWidgets>
#include "mainwindow.h"

MainWindow::MainWindow()
  : imageLabel(new QLabel)
  , scrollArea(new QScrollArea)
  , scaleFactor(1)
{
  imageLabel->setBackgroundRole(QPalette::Base);
  imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
  imageLabel->setScaledContents(true);

  scrollArea->setBackgroundRole(QPalette::Dark);
  scrollArea->setWidget(imageLabel);
  scrollArea->setVisible(false);
  setCentralWidget(scrollArea);

  createActions();
  clear();

  resize(QGuiApplication::primaryScreen()->availableSize());
}

bool MainWindow::loadFile(const QString &fileName)
{
  QImageReader reader(fileName);
  reader.setAutoTransform(true);
  const QImage newImage = reader.read();
  if (newImage.isNull()) {
      QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
                               tr("Cannot load %1: %2")
                               .arg(QDir::toNativeSeparators(fileName), reader.errorString()));
      return false;
    }

  setImage(newImage);
  setWindowFilePath(fileName);

  statusMsg = tr("Loaded \n%1").arg(QDir::toNativeSeparators(fileName));
  updateStatusLabel(statusMsg, LOADED);

  return true;
}

void MainWindow::setImage(const QImage &newImage)
{
  image = newImage;
  imageLabel->setPixmap(QPixmap::fromImage(image));

  scaleFactor = 1.0;

  scrollArea->setVisible(true);
  fitToWindowAct->setEnabled(true);
  updateActions();

  if (!fitToWindowAct->isChecked())
    imageLabel->adjustSize();
}

static void initializeImageFileDialog(QFileDialog &dialog, QFileDialog::AcceptMode acceptMode)
{
  static bool firstDialog = true;

  if (firstDialog)
    {
      firstDialog = false;
      const QStringList picturesLocations = QStandardPaths::standardLocations(QStandardPaths::PicturesLocation);
      dialog.setDirectory(picturesLocations.isEmpty() ? QDir::currentPath() : picturesLocations.last());
    }

  QStringList mimeTypeFilters;
  const QByteArrayList supportedMimeTypes = acceptMode == QFileDialog::AcceptOpen
      ? QImageReader::supportedMimeTypes() : QImageWriter::supportedMimeTypes();

  foreach (const QByteArray &mimeTypeName, supportedMimeTypes)
    mimeTypeFilters.append(mimeTypeName);

  mimeTypeFilters.sort();
  dialog.setMimeTypeFilters(mimeTypeFilters);
  dialog.selectMimeTypeFilter("image/tiff");

  if (acceptMode == QFileDialog::AcceptSave)
    dialog.setDefaultSuffix("tiff");
}

void MainWindow::open()
{
  clear();
  QFileDialog dialog(this, tr("Open File"));
  initializeImageFileDialog(dialog, QFileDialog::AcceptOpen);

  while (dialog.exec() == QDialog::Accepted && !loadFile(dialog.selectedFiles().first())) {}
  imagePath = dialog.selectedFiles().first();
}

void MainWindow::openDirectory()
{
  clear();
  QFileDialog dialog(this, tr("Open Directory"));
  dialog.setFileMode(QFileDialog::Directory);
  dialog.exec();
  dirPath = dialog.selectedFiles().first();

  statusMsg = tr("Loaded directory \n%1").arg(QDir::toNativeSeparators(dirPath));
  updateStatusLabel(statusMsg, LOADED);

  if (dirPath.size()>0)
    f_dirSelected = true;

  updateFileList();
  loadFile(QString(fileList.first()).prepend(dirPath + QDir::separator()));
}

void MainWindow::normalSize()
{
  imageLabel->adjustSize();
  scaleFactor = 1.0;
}



void MainWindow::fitToWindow()
{
  bool fitToWindow = fitToWindowAct->isChecked();
  scrollArea->setWidgetResizable(fitToWindow);
  if (!fitToWindow)
    normalSize();
  updateActions();
}


void MainWindow::clear()
{
  imagePath.clear();
  dirPath.clear();
  fileList.clear();
  fileIdx = 0;
  statusLabel->setStyleSheet("QLabel { background-color : transparent;}");
  imageLabel->clear();
  updateStatusLabel("No files loaded", CLEAR);
  this->setWindowTitle("NCTM");
}

void MainWindow::about()
{
  QMessageBox::about(this, tr("About NCTM"),
                     tr("<p><b>NCTM %1</b><br><br>"
                        "<b>Nanoparticle Characterization Tool for Micrographs</b><br>"
                        "A program to perform morphological charactertization of nanoparticles in SEM micrographs.<br><br>"
                        "Copyright (c) 2021 Jorge López<br><br>"
                        "NCTM makes use of the following projects:"
                        "<ul>"
                        "<li>OpenCV, released under the Apache 2 license. Visit <a href=\"opencv.org\">opencv.org</a></li> for more details."
                        "<li>GOCR, released under the GNU GPL license. Visit <a href=\"jocr.sourceforge.net\">gocr website</a></li> for more details."
                        "</ul>"
                        "</p>").arg(NCTM_VERSION));
}

void MainWindow::createActions()
{
  // File menu
  QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
  QToolBar *actionsToolBar = addToolBar(tr("File"));

  QAction *openAct = fileMenu->addAction(tr("&Open..."), this, &MainWindow::open);
  openAct->setShortcut(tr("CTRL+O"));

  QAction *openDirAct = fileMenu->addAction(tr("&Open Directory..."), this, &MainWindow::openDirectory);
  openDirAct -> setShortcut(tr("CTRL+SHIFT+O"));

  QAction *clearAct = fileMenu->addAction(tr("&Clear..."), this, &MainWindow::clear);
  clearAct -> setShortcut(tr("CTRL+L"));

  fileMenu->addSeparator();

  QAction *exitAct = fileMenu->addAction(tr("E&xit"), this, &QWidget::close);
  exitAct->setShortcut(tr("Ctrl+Q"));

  // Run menu
  QMenu *runMenu = menuBar()->addMenu(tr("&Run"));

  QAction *processMicrographAct = runMenu->addAction(tr("Run on File"), this, &MainWindow::processMicrograph);
  processMicrographAct -> setShortcut(tr("F5"));

  QAction *processDirectoryAct = runMenu->addAction(tr("Run on Directory"), this, &MainWindow::processDirectory);
  processDirectoryAct -> setShortcut(tr("F6"));

  // View menu
  QMenu *viewMenu = menuBar()->addMenu(tr("&View"));

  QAction *nextImageAct = viewMenu->addAction(tr("Next Image"), this, &MainWindow::nextImage);
  nextImageAct -> setShortcut(tr("Right"));

  QAction *prevImageAct = viewMenu->addAction(tr("Previous Image"), this, &MainWindow::prevImage);
  prevImageAct -> setShortcut(tr("Left"));

  viewMenu->addSeparator();

  normalSizeAct = viewMenu->addAction(tr("&Normal Size"), this, &MainWindow::normalSize);
  normalSizeAct->setShortcut(tr("F7"));
  normalSizeAct->setEnabled(false);

  fitToWindowAct = viewMenu->addAction(tr("&Fit to Window"), this, &MainWindow::fitToWindow);
  fitToWindowAct->setEnabled(false);
  fitToWindowAct->setCheckable(true);
  fitToWindowAct->setShortcut(tr("F8"));

  // Help menu
  QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));

  helpMenu->addAction(tr("&About"), this, &MainWindow::about);
  helpMenu->addAction(tr("About &Qt"), this, &QApplication::aboutQt);


  statusLabel = new QLabel;
  statusLabel->setBackgroundRole(QPalette::Base);

  // Add items to toolbar
  actionsToolBar->addAction(openAct);
  actionsToolBar->addAction(openDirAct);
  actionsToolBar->addSeparator();

  actionsToolBar->addAction(processMicrographAct);
  actionsToolBar->addAction(processDirectoryAct);
  actionsToolBar->addSeparator();

  actionsToolBar->addAction(prevImageAct);
  actionsToolBar->addAction(nextImageAct);
  actionsToolBar->addSeparator();

  actionsToolBar->addAction(clearAct);
  actionsToolBar->addSeparator();

  actionsToolBar->addWidget(statusLabel);
}

void MainWindow::updateActions()
{
  normalSizeAct->setEnabled(!fitToWindowAct->isChecked());
}

void MainWindow::updateStatusLabel(QString istr, int msgtype)
{
  switch(msgtype)
    {
    case CLEAR:
      statusLabel->setStyleSheet("QLabel { background-color : transparent; color : black; }");
      break;

    case LOADED:
      statusLabel->setStyleSheet("QLabel { background-color : transparent; color : blue; }");
      break;

    case BUSY:
      statusLabel->setStyleSheet("QLabel { background-color : transparent; color : red; }");
      break;

    case DONE:
      statusLabel->setStyleSheet("QLabel { background-color : transparent; color : green; }");
      break;
    }

  statusLabel->setText(istr);
}


void MainWindow::scaleImage(double factor)
{
  Q_ASSERT(imageLabel->pixmap());
  scaleFactor *= factor;
  imageLabel->resize(scaleFactor * imageLabel->pixmap()->size());

  adjustScrollBar(scrollArea->horizontalScrollBar(), factor);
  adjustScrollBar(scrollArea->verticalScrollBar(), factor);
}

void MainWindow::adjustScrollBar(QScrollBar *scrollBar, double factor)
{
  scrollBar->setValue(int(factor * scrollBar->value()
                          + ((factor - 1) * scrollBar->pageStep()/2)));
}

void MainWindow::updateFileList()
{
  QDir dir(dirPath);
  fileList = dir.entryList();
  fileList.removeOne(".");
  fileList.removeOne("..");
  fileIdx = 0;
  maxFileListIdx = fileList.size()-1;
  imagePath = QString(fileList.at(fileIdx)).prepend(dirPath + QDir::separator());
}

void MainWindow::nextImage()
{
  if (fileIdx++ < maxFileListIdx)
    {
      loadFile(QString(fileList.at(fileIdx)).prepend(dirPath + QDir::separator()));
      imagePath = QString(fileList.at(fileIdx)).prepend(dirPath + QDir::separator());
    }
  else
    fileIdx--;
}

void MainWindow::prevImage()
{
  if (fileIdx-- > 0)
    {
      loadFile(QString(fileList.at(fileIdx)).prepend(dirPath + QDir::separator()));
      imagePath = QString(fileList.at(fileIdx)).prepend(dirPath + QDir::separator());
    }
  else
    fileIdx++;
}

void MainWindow::processMicrograph()
{
  if (imagePath.size() < 1)
  QMessageBox::about(this,"Error",
                     tr("<p>Please load a file first.</p>"));
  else
    {
      NCTNMWorker nctmWorker;
      statusMsg = tr("Processing \n%1").arg(QDir::toNativeSeparators(imagePath));
      updateStatusLabel(statusMsg, BUSY);

      nctmWorker.process_micrograph(imagePath);

      statusMsg = tr("Completed processing for \n%1").arg(QDir::toNativeSeparators(imagePath));
      updateStatusLabel(statusMsg, DONE);
    }
}

void MainWindow::processDirectory()
{
  if (fileList.empty())
  QMessageBox::about(this,"Error",
                     tr("<p>Please load a file first.</p>"));
  else
    {
      QStringList fl;
      foreach (auto file, fileList)
        {
          file.prepend(dirPath + QDir::separator());
          fl.push_back(file);
        }

      NCTNMWorker nctmWorker;
      statusMsg = tr("Processing images in directory \n%1").arg(QDir::toNativeSeparators(dirPath));
      updateStatusLabel(statusMsg, BUSY);

      nctmWorker.process_micrograph(fl);

      statusMsg = tr("Completed processing of images in directory \n%1").arg(QDir::toNativeSeparators(dirPath));
      updateStatusLabel(statusMsg, DONE);
    }
}
